# Echarts-Series-Extend
## 项目介绍
在github上的作者 萧萧(shengbid) 的echarts-series项目上做了一些扩展,主要展示Echarts的基础用法.感谢作者萧萧(shengbid)
如果有帮助请给个star(*^▽^*)
## 萧萧(shengbid)的 git地址
https://github.com/shengbid/echarts-series

## 主要内容
1. Echarts中通用属性设置,还有自定义主题,窗口宽度自适应,数据增量动画等
2. 中国矢量地图数据

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run  build
```
