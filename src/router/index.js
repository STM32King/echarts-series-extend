import Vue from "vue"
import Router from 'vue-router'

Vue.use(Router)

import CommonLayout from "@/layout"

const routes = [{
  path: '/',
  component: CommonLayout,
  redirect: '/titleAttr1',
  children: [
    {
      path: 'titleAttr1',
      component: () => import('@/views/attribute/title'),
      name: 'titleAttr1',
      meta: { parentKey: '1', key: '1-1' }
    },
    {
      path: 'titleAttr2',
      component: () => import('@/views/attribute/grid'),
      name: 'titleAttr2',
      meta: { parentKey: '1', key: '1-2' }
    },
    {
      path: 'xAxisAttr',
      component: () => import('@/views/attribute/xAxis'),
      name: 'xAxisAttr',
      meta: { parentKey: '1', key: '1-3' }
    },
    {
      path: 'yAxisAttr',
      component: () => import('@/views/attribute/yAxis'),
      name: 'yAxisAttr',
      meta: { parentKey: '1', key: '1-4' }
    },
    {
      path: 'seriesAttr',
      component: () => import('@/views/attribute/series'),
      name: 'seriesAttr',
      meta: { parentKey: '1', key: '1-5' }
    },
    {
      path: 'eventChart',
      component: () => import('@/views/attribute/event'),
      name: 'eventChart',
      meta: { parentKey: '1', key: '1-5' }
    },
    {
      path: 'dataZoom',
      component: () => import('@/views/attribute/dataZoom'),
      name: 'dataZoom',
      meta: { parentKey: '1', key: '1-7' }
    },
    {
      path: 'lineChart',
      component: () => import('@/views/charts/line'),
      name: 'lineChart',
      meta: { parentKey: '2', key: '2-1' }
    },
    {
      path: 'barChart',
      component: () => import('@/views/charts/bar'),
      name: 'barChart',
      meta: { parentKey: '2', key: '2-2' }
    },
    {
      path: 'candlestickChart',
      component: () => import('@/views/charts/candlestick'),
      name: 'candlestickChart',
      meta: { parentKey: '2', key: '2-3' }
    },
    {
      path: 'mapChart',
      component: () => import('@/views/charts/map'),
      name: 'mapChart',
      meta: { parentKey: '2', key: '2-4' }
    },
    {
      path: 'effectScatter',
      component: () => import('@/views/charts/effectScatter'),
      name: 'effectScatter',
      meta: { parentKey: '2', key: '2-5' }
    },
    {
      path: 'basicLineChart',
      component: () => import('@/views/charts/line/basicLineChart'),
      name: 'basicLineChart',
      meta: { parentKey: '3', key: '3-1' }
    },
    {
      path: 'basicAreaChart',
      component: () => import('@/views/charts/line/basicAreaChart'),
      name: 'basicAreaChart',
      meta: { parentKey: '3', key: '3-2' }
    },
    {
      path: 'stackedLineChart',
      component: () => import('@/views/charts/line/stackedLineChart'),
      name: 'stackedLineChart',
      meta: { parentKey: '3', key: '3-3' }
    },
    {
      path: 'stackedAreaChart',
      component: () => import('@/views/charts/line/stackedAreaChart'),
      name: 'stackedAreaChart',
      meta: { parentKey: '3', key: '3-4' }
    },
    {
      path: 'multipleYAxes',
      component: () => import('@/views/charts/bar/multipleYAxes'),
      name: 'multipleYAxes',
      meta: { parentKey: '4', key: '4-1' }
    },
    {
      path: 'basicBar',
      component: () => import('@/views/charts/bar/basicBar'),
      name: 'basicBar',
      meta: { parentKey: '4', key: '4-2' }
    },
    {
      path: 'basicPieChart',
      component: () => import('@/views/charts/pie/basicPieChart'),
      name: 'basicPieChart',
      meta: { parentKey: '5', key: '5-1' }
    },
    {
      path: 'basicScatterChart',
      component: () => import('@/views/charts/scatter/basicScatterChart'),
      name: 'basicScatterChart',
      meta: { parentKey: '6', key: '6-1' }
    },
    {
      path: 'baiduMap',
      component: () => import('@/views/charts/map/baiduMap'),
      name: 'baiduMap',
      meta: { parentKey: '7', key: '7-1' }
    },
    {
      path: 'china2',
      component: () => import('@/views/charts/map/china2'),
      name: 'china2',
      meta: { parentKey: '7', key: '7-2' }
    },
    {
      path: 'liaoNing',
      component: () => import('@/views/charts/map/liaoNing'),
      name: 'liaoNing',
      meta: { parentKey: '7', key: '7-3' }
    },
    {
      path: 'basicBarResize',
      component: () => import('@/views/charts/resize/basicBarResize'),
      name: 'basicBarResize',
      meta: { parentKey: '8', key: '8-1' }
    },
    {
      path: 'basicBarResizeTheme',
      component: () => import('@/views/charts/theme/basicBarResizeTheme'),
      name: 'basicBarResizeTheme',
      meta: { parentKey: '9', key: '9-1' }
    },
    {
      path: 'basicBarSetOption',
      component: () => import('@/views/charts/setoption/basicBarSetOption'),
      name: 'basicBarSetOption',
      meta: { parentKey: '10', key: '10-1' }
    },
  ]
}]

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

export default router